use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Configuration {
    pub bandwidth: f64,
    pub hevc: bool,
    pub high_quality_audio: bool,
    pub quick_menu: bool,
    pub fullscreen: bool,
    pub software_decoder: bool,
    pub high_quality_color: bool,
    pub tcp_input : bool
}

impl ::std::default::Default for Configuration {
    fn default() -> Self {
        Self {
            bandwidth:          20.0,
            hevc:               false,
            high_quality_audio: false,
            quick_menu:         false,
            fullscreen:         false,
            software_decoder:   false,
            high_quality_color: false,
            tcp_input :         false
        }
    }
}

impl Configuration {
    pub fn dump(&self)
    {
        confy::store("shadow-launcher", self).expect("Fail");
        println!("Configuration Saved\n{:?}", &self);
    }

    pub fn load() -> Self
    {
        let config: Self = confy::load("shadow-launcher").expect("Fail to get a base config");
        println!("Configuration loaded\n{:?}", config);

        config
    }
}