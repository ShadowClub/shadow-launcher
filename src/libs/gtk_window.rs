extern crate gtk;

use gtk::prelude::*;
use std::clone::Clone;

#[derive(Clone)]
pub struct GtkWindow {
    builder: gtk::Builder,
}

impl GtkWindow {
    pub fn new(template: &str) -> Self 
    {
        let relative_path: String = "../../".to_owned();
        let template2: &str = "assets/glade/home.glade";
        relative_path.push_str(template2);
        println!("{}", relative_path);
        
        let glade_src = include_str!(relative_path);
        let builder: gtk::Builder = gtk::Builder::new_from_string(glade_src);

        GtkWindow { builder }
    }

    pub fn show(&self) 
    {
        let window: gtk::Window = self.builder.get_object("mainWindow").unwrap();

        window.connect_delete_event(|_, _| {
            // Stop the main loop.
            gtk::main_quit();
            // Let the default handler destroy the window.
            Inhibit(false)
        });

        window.show_all();
        gtk::main();    
    }

    pub fn get_bool(&self, id: &str) -> bool
    {
        let switch: gtk::Switch = self.builder.get_object(id).unwrap();

        switch.get_state()
    }

    pub fn set_bool(&self, id: &str, value: bool)
    {
        let switch: gtk::Switch = self.builder.get_object(id).unwrap();

        switch.set_state(value);
    }

    pub fn get_slider_value(&self, id: &str) -> f64
    {
        let adjustement: gtk::Adjustment = self.builder.get_object(id).unwrap();

        adjustement.get_value()
    }

    pub fn set_slider_value(&self, id: &str, value: f64)
    {
        let adjustement: gtk::Adjustment = self.builder.get_object(id).unwrap();

        adjustement.set_value(value);
    }

    pub fn add_button_listener(&self, id: &str, callback: fn(&Self))
    {
        let button: gtk::Button = self.builder.get_object(id).unwrap();
        let cloned_window: Self = self.clone();

        button.connect_clicked(move |_| {
            callback(&cloned_window)
        });
    }

    pub fn add_menu_listener(&self, id: &str, callback: fn(&Self))
    {
        let menu_entry: gtk::MenuItem = self.builder.get_object(id).unwrap();
        let cloned_window: Self = self.clone();

        menu_entry.connect_activate(move |_| {
            callback(&cloned_window)
        });
    }
}