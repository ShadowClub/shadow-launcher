use std::io::prelude::*;
use std::path::Path;
use dirs::*;

pub struct File {}

impl File {
    pub fn write_file(filepath: &str, content: &str)
    {
        let path = Path::new(&filepath);
        let prefix = path.parent().unwrap();
        std::fs::create_dir_all(prefix).unwrap();

        let mut file = std::fs::File::create(filepath).expect("Fail to open the file");

        file.write_all(content.as_bytes()).expect("Fail to write file");
    }

    pub fn read_file(path: &str) -> Result<String, Box<dyn std::error::Error + 'static>>
    {
        let mut file = std::fs::File::open(path)?;
        let mut content = String::new();
        file.read_to_string(&mut content)?;

        Ok(content)
    }

    pub fn get_home_path() -> String
    {
        let home_dir_path = home_dir().expect("Fail to get the home directory");
        let home_dir: &str = home_dir_path.to_str().expect("Fail to parse the home path");

        home_dir.to_owned()
    }
}
