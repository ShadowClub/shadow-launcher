#[path = "libs/config.rs"] mod config;
#[path = "libs/gtk_window.rs"] mod gtk_window;
#[path = "libs/file.rs"] mod file;

use config::Configuration;
use file::File;
use gtk_window::GtkWindow;

pub struct Gui {}

impl Gui {
    pub fn create()
    {
        if gtk::init().is_err() {
            println!("Failed to initialize GTK.");
            return;
        }

        let window: GtkWindow = GtkWindow::new("assets/glade/home.glade");
        let config: Configuration = Configuration::load();
        Self::from_configuration(&window, config);

        window.add_button_listener("connectButton", Self::connect);
        window.add_menu_listener("shortcutButton", Self::create_desktop_file);
        window.add_menu_listener("saveButton", Self::save_configuration);

        window.show();
    }

    fn to_configuration(window: &GtkWindow) -> Configuration
    {
        Configuration {
            bandwidth:          window.get_slider_value("bandwidthAdjustement"),
            hevc:               window.get_bool("hevcToggle"),
            high_quality_audio: window.get_bool("highQualityAudioToggle"),
            quick_menu:         window.get_bool("quickMenuToggle"),
            fullscreen:         window.get_bool("fullscreenToggle"),
            software_decoder:   window.get_bool("softwareDecoderToggle"),
            high_quality_color: window.get_bool("highQualityColorToggle"),
            tcp_input :         window.get_bool("tcpInputToggle")
        }
    }

    fn from_configuration(window: &GtkWindow, config: Configuration)
    {
        window.set_slider_value("bandwidthAdjustement", config.bandwidth);
        window.set_bool("hevcToggle", config.hevc);
        window.set_bool("highQualityAudioToggle", config.high_quality_audio);
        window.set_bool("quickMenuToggle", config.quick_menu);
        window.set_bool("fullscreenToggle", config.fullscreen);
        window.set_bool("softwareDecoderToggle", config.software_decoder);
        window.set_bool("highQualityColorToggle", config.high_quality_color);
        window.set_bool("tcpInputToggle", config.tcp_input);
    }

    fn connect(window: &GtkWindow)
    {
        let config = Self::to_configuration(window);
        println!("{:?}", config);
    }

    fn create_desktop_file(_: &GtkWindow)
    {
        File::write_file(
            "/.local/share/applications/shadow-launcher.desktop",
            "
                [Desktop Entry]
                Name=Shadow Beta
                Comment=Play your games in streaming from your Shadow
                Comment[fr]=Jouez a vos jeux en streaming avec votre Shadow
                Comment[de]=Spiele deine Spiele im Streaming von deinem Shadow
                Exec=shadow-preprod
                Terminal=false
                Type=Application
                Icon=shadow-beta
                Categories=Games;Virtualization
            "
        );

        println!("Shortcut created!");
    }

    fn save_configuration(window: &GtkWindow)
    {
        let config: Configuration = Self::to_configuration(window);
        config.dump();
    }
}
