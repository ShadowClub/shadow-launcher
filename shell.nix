{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  name = "shadow-launcher";

  nativeBuildInputs = with pkgs; [ 
    rustc 
    cargo 
    pkgconfig
    gcc 
    rustup

    bat
    gtkd
    atk
    cairo
    gdk_pixbuf
    glib
    gnome2.pango
  ];

  buildInputs = with pkgs; [ rustfmt clippy glade ];

  # See https://discourse.nixos.org/t/rust-src-not-found-and-other-misadventures-of-developing-rust-on-nixos/11570/3?u=samuela.
  # RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
}
