> :warning: Since I canceled my subscription, I am no longer able to maintain this package. If somebody wants to continue the maintenance, feel free to fork or [contact me](mailto:nicolas.guilloux@protonmail.com)

# Shadow Launcher

This is an alternative launcher for the Shadow application on Linux. The goal is to start faster your stream without the hassle of having a launcher that slows your experience.

Here is 2 sketches of what the launcher would be the launcher like:

![Home sketch](doc/images/home_sketch.jpg)
![Config sketch](doc/images/config_sketch.jpg)


## Features

- Choose any option like the actual launcher.
- Switch directly from the launcher the branch you want.
- Give you information about each steps during the boot process and the time it takes.
- Create a shortcut to directly start the renderer with the saved configuration.
